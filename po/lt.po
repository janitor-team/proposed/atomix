# Lithuanian translation of atomix.
# Copyright (C) 2005 Free Software Foundation
# This file is distributed under the same license as the atomix package.
# Žygimantas Beručka <zygis@gnome.org>, 2005.
# Mantas Kriaučiūnas <mantas@akl.lt>, 2015.
# Aurimas Černius <aurisc4@gmail.com>, 2016-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: atomix HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/atomix/issues\n"
"POT-Creation-Date: 2018-10-07 20:07+0000\n"
"PO-Revision-Date: 2018-11-25 18:07+0200\n"
"Last-Translator: Aurimas Černius <aurisc4@gmail.com>\n"
"Language-Team: Lietuvių <gnome-lt@lists.akl.lt>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 3.30.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && (n"
"%100<10 || n%100>=20) ? 1 : 2);\n"

#: data/atomix.appdata.xml.in:7 data/atomix.desktop.in:3 src/main.c:121
#: src/main.c:688
msgid "Atomix"
msgstr "Atomix"

#: data/atomix.appdata.xml.in:8
msgid "Build molecules out of single atoms"
msgstr "Sudėkite įvairias molekules iš atskirų atomų"

#: data/atomix.appdata.xml.in:10
msgid ""
"Atomix is a puzzle game where your goal is to assemble molecules from "
"compound atoms by moving them on the playfield. However, atoms don’t just "
"move wherever you want to move them to, they slide until they hit either a "
"wall or another atom."
msgstr ""
"Atomix yra galvosūkio žaidimas, kurio tikslas yra sudėti molekules iš atomų, "
"juos stumdant lauke. Tačiau atomai nejuda jums panorėjus, jie slenka iki "
"atsitrenkia į sieną arba vienas į kitą."

#: data/atomix.appdata.xml.in:16
msgid ""
"Try to build the molecules as fast as you can on each level to earn a higher "
"score."
msgstr ""
"Bandykite sudaryti molekules kiek galima greičiau kiekvienam lygiui ir "
"gaukite daugiau taškų."

#: data/atomix.desktop.in:4
msgid "Molecule puzzle game"
msgstr "Molekulių galvosūkis"

#: data/atomix.desktop.in:5 src/main.c:124
msgid "A puzzle game about atoms and molecules"
msgstr "Galvosūkis apie atomus ir molekules"

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/atomix.desktop.in:8
msgid "atomix"
msgstr "atomix"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/atomix.desktop.in:13
msgid "GNOME;game;logic;puzzle;atoms;molecules;sokoban;"
msgstr "GNOME;žaidimas;logika;galvosūkis;atomai;molekulės;sokoban;"

#: data/level/aceticacid.atomix.xml.in:2
msgid "Acetic Acid"
msgstr "Acto rūgštis"

#: data/level/acetone.atomix.xml.in:2
msgid "Acetone"
msgstr "Acetonas"

#: data/level/butanol.atomix.xml.in:2
msgid "Butanol"
msgstr "Butanolis"

#: data/level/cyclobutane.atomix.xml.in:2
msgid "Cyclobutane"
msgstr "Ciklobutanas"

#: data/level/dimethylether.atomix.xml.in:2
msgid "Dimethyl Ether"
msgstr "Dimetil eteris"

#: data/level/ethanal.atomix.xml.in:2
msgid "Ethanal"
msgstr "Etanalis"

#: data/level/ethane.atomix.xml.in:2
msgid "Ethane"
msgstr "Etanas"

#: data/level/ethanol.atomix.xml.in:2
msgid "Ethanol"
msgstr "Etanolis"

#: data/level/ethylene.atomix.xml.in:2
msgid "Ethylene"
msgstr "Etilenas"

#: data/level/glycerin.atomix.xml.in:2
msgid "Glycerin"
msgstr "Glicerinas"

#: data/level/lactic-acid.atomix.xml.in:2
msgid "Lactic Acid"
msgstr "Pieno rūgštis"

#: data/level/methanal.atomix.xml.in:2
msgid "Methanal"
msgstr "Metanalis"

#: data/level/methane.atomix.xml.in:2
msgid "Methane"
msgstr "Metanas"

#: data/level/methanol.atomix.xml.in:2
msgid "Methanol"
msgstr "Metanolis"

#: data/level/propanal.atomix.xml.in:2
msgid "Propanal"
msgstr "Propanalis"

#: data/level/propylene.atomix.xml.in:2
msgid "Propylene"
msgstr "Propilenas"

#: data/level/pyran.atomix.xml.in:2
msgid "Pyran"
msgstr "Piranas"

#: data/level/transbutylen.atomix.xml.in:2
msgid "Trans Butylen"
msgstr "Trans-butilenas"

#: data/level/water.atomix.xml.in:2
msgid "Water"
msgstr "Vanduo"

#: data/ui/interface.ui:99
msgid "Level:"
msgstr "Lygis:"

#: data/ui/interface.ui:111
msgid "Molecule:"
msgstr "Molekulė:"

#: data/ui/interface.ui:123
msgid "Formula:"
msgstr "Formulė:"

#: data/ui/interface.ui:135
msgid "Score:"
msgstr "Taškai:"

#: data/ui/interface.ui:147
msgid "Time:"
msgstr "Laikas:"

#: data/ui/interface.ui:159 data/ui/interface.ui:170 data/ui/interface.ui:182
#: data/ui/interface.ui:194
msgid "empty"
msgstr "tuščia"

#: data/ui/interface.ui:213
msgid "Statistics"
msgstr "Statistika"

#: data/ui/menu.ui:7
msgid "New Game"
msgstr "Naujas žaidimas"

#: data/ui/menu.ui:12
msgid "Pause/Continue Game"
msgstr "Pristabdyti/tęsti žaidimą"

#: data/ui/menu.ui:17
msgid "End Game"
msgstr "Baigti žaidimą"

#: data/ui/menu.ui:23
msgid "Skip Level"
msgstr "Praleisti lygį"

#: data/ui/menu.ui:28
msgid "Reset Level"
msgstr "Pradėti lygį iš naujo"

#: data/ui/menu.ui:32
msgid "Undo Move"
msgstr "Atšaukti ėjimą"

#: data/ui/menu.ui:39
#| msgid "Atomix"
msgid "About Atomix"
msgstr "Apie Atomix"

#: src/board-gtk.c:340
msgid ""
"Guide the atoms through the maze to form molecules. Click, or use the arrow "
"keys and Enter, to select an atom and move it. Be careful, though: an atom "
"keeps moving until it hits a wall."
msgstr ""
"Lydėkite atomus per labirintą, kad suformuotumėte molekules. Spustelėkite "
"arba naudokite krypties klavišus ir įvedimo klavišą, kad pasirinktumėte "
"atomą ir jį perkeltumėte. Tačiau būkite atsargūs: atomas juda tol, kol "
"neatsiremia į sieną."

#: src/main.c:128
msgid "translator-credits"
msgstr ""
"Žygimantas Beručka <zygis@gnome.org>\n"
"Mantas Kriaučiūnas <mantas@akl.lt>"

#: src/main.c:447
msgid "Congratulations! You have finished all Atomix levels."
msgstr "Sveikiname! Įveikėte visus Atomix lygius."

#~ msgid "About"
#~ msgstr "Apie"

#~ msgid "Quit"
#~ msgstr "Išeiti"

#~ msgid "_File"
#~ msgstr "Ž_aidimas"

#~ msgid "_New Game"
#~ msgstr "_Naujas žaidimas"

#~ msgid "_End Game"
#~ msgstr "_Baigti žaidimą"

#~ msgid "_Skip Level"
#~ msgstr "Pra_leisti lygį"

#~ msgid "_Reset Level"
#~ msgstr "_Pradėti lygį iš naujo"

#~ msgid "_Undo Move"
#~ msgstr "Atša_ukti ėjimą"

#~ msgid "_Pause Game"
#~ msgstr "Pri_stabdyti žaidimą"

#~ msgid "_Continue Game"
#~ msgstr "_Tęsti žaidimą"

#~ msgid "_Quit"
#~ msgstr "_Išeiti"

#~ msgid "_Help"
#~ msgstr "_Žinynas"

#~ msgid "_About"
#~ msgstr "_Apie"

#~ msgid "Pause Game"
#~ msgstr "Pristabdyti žaidimą"

#~ msgid "Couldn't find level sequence description."
#~ msgstr "Nepavyko rasti lygių eilės aprašymo."

#~ msgid "No level found."
#~ msgstr "Lygių nerasta."

#~ msgid "Found level '%s' in: %s"
#~ msgstr "Rastas lygis „%s“ esantis: %s"

#~ msgid "No themes found."
#~ msgstr "Temų nerasta."

#~ msgid "Found theme '%s' in: %s"
#~ msgstr "Rasta tema „%s“ esanti: %s"

#~ msgid "Could not show link"
#~ msgstr "Nepavyko parodyti saito"

#~| msgid "Time:"
#~ msgid "Time"
#~ msgstr "Laikas"

#~| msgid "Score:"
#~ msgid "Score"
#~ msgstr "Rezultatas"

#~ msgid "%dm %ds"
#~ msgstr "%dm %ds"

#~| msgid "_Game"
#~ msgid "Name"
#~ msgstr "Vardas"

#~ msgid "Error"
#~ msgstr "Klaida"

#~ msgid "Couldn't find at least one level."
#~ msgstr "Nepavyko rasti nė vieno lygio."

#~ msgid "Do you want to finish the game?"
#~ msgstr "Ar tikrai norite užbaigti žaidimą?"

#~ msgid "_Game"
#~ msgstr "_Žaidimas"

#~| msgid "_Scores ..."
#~ msgid "_Scores..."
#~ msgstr "_Rezultatai..."

#~ msgid "Continue paused game"
#~ msgstr "Tęsti sustabdytą žaidimą"

#~ msgid "End a game"
#~ msgstr "Baigti žaidimą"

#~ msgid "Pause the running game"
#~ msgstr "Sustabdyti esamą žaidimą"

#~ msgid "Restores start situation"
#~ msgstr "Atkuria pradinę situaciją"

#~ msgid "Set preferences"
#~ msgstr "Keisti nustatymus"

#~ msgid "Skip the current level"
#~ msgstr "Praleisti šį lygį"

#~ msgid "Start a new game"
#~ msgstr "Pradėti naują žaidimą"

#~ msgid "Undo the last move"
#~ msgstr "Atšaukti paskutinį ėjimą"

#~ msgid "View highscores"
#~ msgstr "Žiūrėti rekordus"

#~ msgid "_Preferences ..."
#~ msgstr "N_ustatymai..."

#~ msgid ""
#~ "You have not achieved any scores yet. Play a little before coming back!"
#~ msgstr ""
#~ "Dar nepasiekėte jokių rezultatų. Pažaiskite daugiau ir grįžkite vėliau!"

#~ msgid "Couldn't find file: %s"
#~ msgstr "Nepavyko rasti failo: %s"
