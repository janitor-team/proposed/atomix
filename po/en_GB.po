# English (British) translation.
# Copyright (C) 2004 THE atomix'S COPYRIGHT HOLDER
# This file is distributed under the same license as the atomix package.
# Gareth Owen <gowen72@yahoo.com>, 2004.
# Chris Leonard <cjl@laptop.org>, 2012.
# Bruce Cowan <bruce@bcowan.me.uk>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: atomix\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=atomix"
"&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-07-12 23:38+0000\n"
"PO-Revision-Date: 2012-09-14 15:28+0100\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: British English <en@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"
"X-Project-Style: gnome\n"

#: ../level/aceticacid.atomix.xml.h:1
msgid "Acetic Acid"
msgstr "Acetic Acid"

#: ../level/acetone.atomix.xml.h:1
msgid "Acetone"
msgstr "Acetone"

#: ../level/butanol.atomix.xml.h:1
msgid "Butanol"
msgstr "Butanol"

#: ../level/cyclobutane.atomix.xml.h:1
msgid "Cyclobutane"
msgstr "Cyclobutane"

#: ../level/dimethylether.atomix.xml.h:1
msgid "Dimethyl Ether"
msgstr "Dimethyl Ether"

#: ../level/ethanal.atomix.xml.h:1
msgid "Ethanal"
msgstr "Ethanal"

#: ../level/ethane.atomix.xml.h:1
msgid "Ethane"
msgstr "Ethane"

#: ../level/ethanol.atomix.xml.h:1
msgid "Ethanol"
msgstr "Ethanol"

#: ../level/ethylene.atomix.xml.h:1
msgid "Ethylene"
msgstr "Ethylene"

#: ../level/glycerin.atomix.xml.h:1
msgid "Glycerin"
msgstr "Glycerin"

#: ../level/lactic-acid.atomix.xml.h:1
msgid "Lactic Acid"
msgstr "Lactic Acid"

#: ../level/methanal.atomix.xml.h:1
msgid "Methanal"
msgstr "Methanal"

#: ../level/methane.atomix.xml.h:1
msgid "Methane"
msgstr "Methane"

#: ../level/methanol.atomix.xml.h:1
msgid "Methanol"
msgstr "Methanol"

#: ../level/propanal.atomix.xml.h:1
msgid "Propanal"
msgstr "Propanal"

#: ../level/propylene.atomix.xml.h:1
msgid "Propylene"
msgstr "Propylene"

#: ../level/pyran.atomix.xml.h:1
msgid "Pyran"
msgstr "Pyran"

#: ../level/transbutylen.atomix.xml.h:1
msgid "Trans Butylen"
msgstr "Trans Butylen"

#: ../level/water.atomix.xml.h:1
msgid "Water"
msgstr "Water"

#: ../src/games-runtime.c:273
msgid "Could not show link"
msgstr "Could not show link"

#: ../src/games-scores-dialog.c:113
msgid "Time"
msgstr "Time"

#. Note that this assumes the default style is plain.
#: ../src/games-scores-dialog.c:118 ../src/games-scores-dialog.c:545
msgid "Score"
msgstr "Score"

#. Translators: this is for a minutes, seconds time display.
#: ../src/games-scores-dialog.c:276
#, c-format
msgid "%dm %ds"
msgstr "%dm %ds"

#: ../src/games-scores-dialog.c:437 ../src/main.c:736
msgid "New Game"
msgstr "New Game"

#: ../src/games-scores-dialog.c:537
msgid "Name"
msgstr "Name"

#. Empty title shows up as "<unnamed>" on maemo
#: ../src/games-show.c:151
msgid "Error"
msgstr "Error"

#: ../src/level-manager.c:176
msgid "Couldn't find level sequence description."
msgstr "Couldn't find level sequence description."

#: ../src/level-manager.c:190
msgid "No level found."
msgstr "No level found."

#: ../src/level-manager.c:286
#, c-format
msgid "Found level '%s' in: %s"
msgstr "Found level '%s' in: %s"

#: ../src/main.c:121 ../src/main.c:775 ../src/main.c:832
#: ../atomix.desktop.in.h:1
msgid "Atomix"
msgstr "Atomix"

#: ../src/main.c:166
msgid "A puzzle game about atoms and molecules"
msgstr "A puzzle game about atoms and molecules"

#: ../src/main.c:170
msgid "translator-credits"
msgstr ""
"Gareth Owen <gowen@yahoo.com>\n"
"David Lodge <dave@cirt.net>\n"
"Chris Leonard <cjl@laptop.org>"

#: ../src/main.c:493
msgid "Congratulations! You have finished all Atomix levels."
msgstr "Congratulations! You have finished all Atomix levels."

#: ../src/main.c:503
msgid "Couldn't find at least one level."
msgstr "Couldn't find at least one level."

#: ../src/main.c:508
msgid "Do you want to finish the game?"
msgstr "Do you want to finish the game?"

#. create statistics frame
#: ../src/main.c:701
msgid "Statistics"
msgstr "Statistics"

#: ../src/main.c:707
msgid "Level:"
msgstr "Level:"

#: ../src/main.c:708
msgid "Molecule:"
msgstr "Molecule:"

#: ../src/main.c:709
msgid "Formula:"
msgstr "Formula:"

#: ../src/main.c:710
msgid "Score:"
msgstr "Score:"

#: ../src/main.c:711
msgid "Time:"
msgstr "Time:"

#: ../src/main.c:734
msgid "_Game"
msgstr "_Game"

#: ../src/main.c:735
msgid "_Help"
msgstr "_Help"

#: ../src/main.c:737
msgid "End Game"
msgstr "End Game"

#: ../src/main.c:738
msgid "Skip Level"
msgstr "Skip Level"

#: ../src/main.c:739
msgid "Reset Level"
msgstr "Reset Level"

#: ../src/main.c:741
msgid "_Pause Game"
msgstr "_Pause Game"

#: ../src/main.c:742
msgid "_Continue Game"
msgstr "_Continue Game"

#: ../src/main.c:743
msgid "_Scores..."
msgstr "_Scores…"

#: ../src/main.c:745
msgid "About"
msgstr "About"

#: ../src/theme-manager.c:136
msgid "No themes found."
msgstr "No themes found."

#: ../src/theme-manager.c:194
#, c-format
msgid "Found theme '%s' in: %s"
msgstr "Found theme '%s' in: %s"

#: ../atomix.desktop.in.h:2
msgid "Molecule puzzle game"
msgstr "Molecule puzzle game"

#~ msgid "Continue paused game"
#~ msgstr "Continue paused game"

#~ msgid "End a game"
#~ msgstr "End a game"

#~ msgid "Pause the running game"
#~ msgstr "Pause the running game"

#~ msgid "Restores start situation"
#~ msgstr "Restores start situation"

#~ msgid "Set preferences"
#~ msgstr "Set preferences"

#~ msgid "Skip the current level"
#~ msgstr "Skip the current level"

#~ msgid "Start a new game"
#~ msgstr "Start a new game"

#~ msgid "Undo the last move"
#~ msgstr "Undo the last move"

#~ msgid "View highscores"
#~ msgstr "View highscores"

#~ msgid "_Edit"
#~ msgstr "_Edit"

#~ msgid "_Preferences ..."
#~ msgstr "_Preferences ..."

#~ msgid "_Undo move"
#~ msgstr "_Undo move"

#~ msgid ""
#~ "You have not achieved any scores yet. Play a little before coming back!"
#~ msgstr ""
#~ "You have not achieved any scores yet. Play a little before coming back!"

#~ msgid "Couldn't find file: %s"
#~ msgstr "Couldn't find file: %s"

#~ msgid "E_xit"
#~ msgstr "E_xit"

#~ msgid "Exit game"
#~ msgstr "Exit game"

#~ msgid "View game credits"
#~ msgstr "View game credits"

#~ msgid "Copyright (C) 1999-2002 Jens Finke"
#~ msgstr "Copyright (C) 1999-2002 Jens Finke"

#~ msgid "Couldn't create directory: %s"
#~ msgstr "Couldn't create directory: %s"
